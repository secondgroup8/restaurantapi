package main

import (
	"log"
	"rest/config"
	"rest/repository/postgres"
	"rest/server"
)

func main() {
	cfg, err := config.Load()
	if err != nil {
		log.Fatal(err)
	}
	db, err := postgres.Connect(cfg)
	if err != nil {
		log.Fatal(err)
	}
	p := postgres.New(db)
	server.HandlerRequest(server.NewHandler(p))
}
