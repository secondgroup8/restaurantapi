package server

import "github.com/gin-gonic/gin"

type ServerHandler struct {
	Repo Repository
}

func NewHandler(u Repository) ServerHandler{
 return ServerHandler{
	Repo: u, 
 }
}

func (s ServerHandler) Login(e *gin.Context)  {
	
}

func (s ServerHandler) Budget(e *gin.Context)  {
	
}

func (s ServerHandler) Option(e *gin.Context)  {
	
}

func (s ServerHandler) Category(e *gin.Context)  {
	
}

func (s ServerHandler) Payment(e *gin.Context)  {
	
}

func (s ServerHandler) Meal(e *gin.Context) {

}

func (s ServerHandler) AddtoOrder(e *gin.Context) {
	
}

func (s ServerHandler) Meals(e *gin.Context) {

}

func (s ServerHandler) Ingredient(e *gin.Context) {

}

func (s ServerHandler) Order(e *gin.Context) {

}

func (s ServerHandler) LogAdmin(e *gin.Context) {

}

func (s ServerHandler) CafeBudget(e *gin.Context) {

}

func (s ServerHandler) UpdateMeal(e *gin.Context) {

}

func (s ServerHandler) NewTable(e *gin.Context) {

}

func (s ServerHandler) NewMeal(e *gin.Context) {

}