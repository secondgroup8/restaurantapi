package server

import (
	"context"
	"rest/entity/history"
	"rest/entity/meal"
	"rest/entity/order"
	"rest/entity/serverEntity"
)

type Repository interface {
	Login(c context.Context, ph string) error
	Budget(c context.Context, id string) (int, error)
	Option(c context.Context, op int) ([]meal.Meal, error)
	Category(c context.Context) (meal.Category, error)
	Payment(c context.Context, id string) error
	Meal(c context.Context, id string) (meal.Meal, error)
	AddtoOrder(c context.Context, m serverEntity.MealRequest) error
	Meals(c context.Context, cId string) ([]meal.Meal, error)
	Ingredient(c context.Context, id string) (meal.Ingredient, error)
	Order(c context.Context, id string) (order.Order, error)
	LogAdmin(c context.Context, pas string) error
	CafeBudget(c context.Context) (int, error)
	History(c context.Context, id string) (history.History, error)
	UpdateMeal(c context.Context, s serverEntity.UpdateMealRequest) error
	NewTable(c context.Context) error
	NewMeal(c context.Context) error
}
