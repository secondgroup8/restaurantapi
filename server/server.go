package server

import (
	"github.com/gin-gonic/gin"
)

func HandlerRequest(r Server) gin.Engine {
	e := gin.Default()
	u := e.Group("/user")
	u.POST("/login", r.Login)
	u.GET("/budget", r.Budget)
	u.GET("/option", r.Option)
	u.GET("/category", r.Category)
	u.POST("/order/payment:id", r.Payment)
	u.GET("/category/mealist/meal", r.Meal)
	u.POST("/category/mealist/meal", r.AddtoOrder)
	u.GET("/category/mealist", r.Meals)
	u.GET("/category/mealist/meal/ingredient", r.Ingredient)
	u.GET("/order", r.Order)
	a := e.Group("/admin")
	a.POST("/login", r.LogAdmin)
	a.GET("/budget", r.CafeBudget)
	a.PUT("/update", r.UpdateMeal)
	a.POST("/newtable", r.NewTable)
	a.POST("/newmeal", r.NewMeal)

	return *e
}


type Server interface {
	Login(e *gin.Context)
	Budget(e *gin.Context)
	Option(e *gin.Context)
	Category(e *gin.Context)
	Payment(e *gin.Context)
	Meal(e *gin.Context)
	AddtoOrder(e *gin.Context)
	Meals(e *gin.Context)
	Ingredient(e *gin.Context)
	Order(e *gin.Context)
	LogAdmin(e *gin.Context)
	CafeBudget(e *gin.Context)
	UpdateMeal(e *gin.Context)
	NewTable(e *gin.Context)
	NewMeal(e *gin.Context) 
}
