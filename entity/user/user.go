package user

import (
	"github.com/google/uuid"
)

type User struct {
	ID       string
	PhoneNum string
	Budget   int
}

func New() User{
	return User{
		ID: uuid.NewString(),
	}
}
