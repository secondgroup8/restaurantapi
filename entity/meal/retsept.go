package meal

import "github.com/google/uuid"

type Retsept struct {
	ID string
	Meal Meal
}

func NewRetsept() Retsept{
	return Retsept{
		ID: uuid.NewString(),
	}
}
