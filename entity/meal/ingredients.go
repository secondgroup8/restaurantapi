package meal

import (
	"time"

	"github.com/google/uuid"
)

type Ingredient struct {
	ID string
	Name string
	ComingTime time.Time
}

func NewIngredient() Ingredient{
	return Ingredient{
		ID: uuid.NewString(),
	}
}