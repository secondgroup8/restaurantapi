package meal

import "github.com/google/uuid"

type Meal struct {
	ID string
	Name string
	Category Category
	Price int
}

func New() Meal{
	return Meal{
		ID: uuid.NewString(),
	}
}
