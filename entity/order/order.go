package order

import (
	"rest/entity/cafe"
	"rest/entity/meal"
	"rest/entity/user"

	"github.com/google/uuid"
)

type Order struct {
	ID string
	User user.User
	Meal meal.Meal
	Table cafe.Table
	Cash int
	Status bool
}

func New() Order{
	return Order{
		ID: uuid.NewString(),
	}
}
