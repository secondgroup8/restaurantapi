package cafe

import "github.com/google/uuid"

type Table struct {
	ID     string
	Budget int
	Status bool
}

func NewTable() Table{
	return Table{
		ID: uuid.NewString(),
	}
}
