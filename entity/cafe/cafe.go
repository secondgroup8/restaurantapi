package cafe

type Cafe struct {
	ID string
	Name string
	Budget int
}