package history

import (
	"rest/entity/user"
	"time"

	"github.com/google/uuid"
)

type History struct {
	ID string
	User user.User
	Cash int
	Time  time.Time
}

func New() History{
	return History{
		ID: uuid.NewString(),
	}
}
