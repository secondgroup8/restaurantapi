package serverEntity

type MealRequest struct {
	ID string `json:"id"`
	CategoryID string `json:"category_id"`
	Quantity int `json:"quantity"`
}

type UpdateMealRequest struct {
	ID string `json:"id"`
	Name string `json:"name"`
	CategoryID string `json:"category_id"`
	Price int `json:"price"`
}