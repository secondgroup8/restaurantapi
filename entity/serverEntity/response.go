package serverEntity

import "rest/entity/meal"

type Order struct {
	ID string `json:"id"`
	Meals []meal.Meal `json:"meal_list"`
	TableID string `json:"table_id"`
	Cash int `json:"cash"`
}

type Menu struct {
	Meals []meal.Meal `json:"meal_list"`
}

type Option struct{
	Meals []meal.Meal `json:"meal_list"`
	Price int `json:"price"`
}