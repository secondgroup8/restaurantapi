package postgres

import (
	"context"
	"database/sql"
	"rest/entity/history"
	"rest/entity/meal"
	"rest/entity/order"
	"rest/entity/serverEntity"
)

type Repository struct {
	DB *sql.DB
}

func New(db *sql.DB) Repository {
	return Repository{
		db,
	}
}

func (r Repository) Login(ctx context.Context, p string) error {
	return nil
}

func (r Repository) Budget(ctx context.Context, id string ) (int, error) {
	return 0,nil
}

func (r Repository) Option(ctx context.Context, m int) ([]meal.Meal, error) {
	return []meal.Meal{}, nil
}

func (r Repository) Category(ctx context.Context) (meal.Category, error) {
	return meal.Category{}, nil
}

func (r Repository) Meal(ctx context.Context, id string) (meal.Meal, error){
	return meal.Meal{}, nil
}

func (r Repository) Payment(ctx context.Context, id string) error {
	return nil
}

func (r Repository) AddtoOrder(c context.Context, m serverEntity.MealRequest) error{
	return nil
}

func (r Repository) Meals(c context.Context, cId string) ([]meal.Meal, error) {
	return []meal.Meal{}, nil
}

func (r Repository) Ingredient(c context.Context, id string) (meal.Ingredient, error) {
	return meal.Ingredient{}, nil
}

func (r Repository) Order(c context.Context, id string) (order.Order, error) {
	return order.Order{}, nil
}

func (r Repository) LogAdmin(c context.Context, password string) error{
	return nil
}

func (r Repository) CafeBudget(c context.Context) (int, error) {
	return 0, nil
}

func (r Repository) History(c context.Context, id string) (history.History, error) {
	return history.History{}, nil
}

func (r Repository) UpdateMeal (c context.Context, s serverEntity.UpdateMealRequest) error{
	return nil
}

func (r Repository) NewTable (c context.Context) error {
	return nil
}

func (r Repository) NewMeal(c context.Context) error {
	return nil
}
