
create table users (
    id uuid primary key,
    phone_num varchar(20),
    budget int default 0);

create table categories (
    id uuid primary key,
    name varchar(40));

create table meals (
    id uuid primary key,
    name varchar(40),
    category_id uuid references categories(id),
    price int not null );

create table tables(
    id uuid  primary key,
    budget int,
    status boolean default false);

create table orders (
    id uuid primary key ,
    user_id uuid references users(id),
    meal_id uuid references meals(id),
    table_id uuid references tables(id),
    cash int not null ,
    status boolean default false );

create table ingredients (
    id  uuid primary key ,
    name varchar(20),
    coming_time date default current_date );

create table receipts (
    id uuid primary key ,
    meal_id uuid references meals(id),
    ingredients uuid[] );

create table histories(
    id uuid primary key ,
    user_id uuid references users(id),
    cash int,
    time timestamp default current_timestamp );

create table cafe(
    id uuid primary key ,
    name varchar(30),
    budget int default 0);

create table admins (
    id uuid  primary key ,
    password varchar(10));

